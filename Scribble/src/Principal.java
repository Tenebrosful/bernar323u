import java.awt.Dimension;

import javax.swing.JFrame;

/**
 * 
 */

/**
 * @author BERNARD Hugo 'Tenebrosful'
 *
 */
public class Principal {

	/**
	 *
	 * @param args
	 */
	public static void main(String[] args) {
		JFrame fenetre = new JFrame("Scribble");
		fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Scribble scr = new Scribble();
		scr.setPreferredSize(new Dimension(500,500));
		fenetre.setContentPane(scr);
		fenetre.pack();
		fenetre.setVisible(true);
		scr.requestFocusInWindow();
	}

}

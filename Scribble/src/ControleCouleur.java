import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * 
 */

/**
 * @author BERNARD Hugo 'Tenebrosful'
 *
 */
public class ControleCouleur implements KeyListener {
	
	Scribble scri;

	/**
	 * Constructeur de ControleCouleur.java pour 
	 * @param scri
	 */
	public ControleCouleur(Scribble scri) {
		this.scri = scri;
	}

	@Override
	public void keyPressed(KeyEvent arg0) {	
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		char k = arg0.getKeyChar();
		
		if(k == 'r')
			this.scri.changerCouleur(Color.RED);
		
		if(k == 'v')
			this.scri.changerCouleur(Color.GREEN);
		
		if(k == 'b')
			this.scri.changerCouleur(Color.BLUE);;
		
		if(k == 'j')
			this.scri.changerCouleur(Color.YELLOW);;
		
		if(k == 'g')
			this.scri.changerCouleur(Color.GRAY);;
		
		if(k == 'm')
			this.scri.changerCouleur(new Color(255,0,255));
		
		if(k == 'z')
			this.scri.changerCouleur(Color.PINK);
		
		if(k == 'n')
			this.scri.changerCouleur(Color.BLACK);
		
	}

}

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;

/**
 * 
 */

/**
 * @author BERNARD Hugo 'Tenebrosful'
 *
 */
public class Scribble extends JPanel {

	private int x, y, x0, y0;
	private Color c = Color.BLACK;

	/**
	 * Constructeur de Scribble.java pour
	 */
	public Scribble() {

		JPanel jpBoutons = new JPanel();

		jpBoutons.add(new JButton("Effacer 🗑"));

		JComboBox<String> jcb = new JComboBox<String>(
				new String[] { "Rouge", "Vert", "Bleu", "Jaune", "Gris", "Violet", "Rose", "Noir" });
		jcb.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				switch (jcb.getSelectedIndex()) {
				case 0:
					changerCouleur(Color.RED);
					break;
				case 1:
					changerCouleur(Color.GREEN);
					break;
				case 2:
					changerCouleur(Color.BLUE);
					break;
				case 3:
					changerCouleur(Color.YELLOW);
					break;
				case 4:
					changerCouleur(Color.GRAY);
					break;
				case 5:
					changerCouleur(new Color(255, 0, 255));
					break;
				case 6:
					changerCouleur(Color.PINK);
					break;
				case 7:
					changerCouleur(Color.BLACK);
					break;
				default:
					changerCouleur(Color.BLACK);
				}
			}

		});
		jpBoutons.add(jcb);

		MouseListener ml = new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent arg0) {
			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
			}

			@Override
			public void mouseExited(MouseEvent arg0) {
			}

			@Override
			public void mousePressed(MouseEvent arg0) {
				System.out.println(arg0);
				x0 = arg0.getX();
				y0 = arg0.getY();
			}

			@Override
			public void mouseReleased(MouseEvent arg0) {
			}
		};

		addMouseListener(ml);

		MouseMotionListener mml = new MouseMotionListener() {

			@Override
			public void mouseDragged(MouseEvent arg0) {
				System.out.println(arg0);
				x = arg0.getX();
				y = arg0.getY();
				repaint();
			}

			@Override
			public void mouseMoved(MouseEvent arg0) {
			}

		};

		addMouseMotionListener(mml);
	}

	public void paintComponent(Graphics g) {
		// super.paintComponent(g);
		g.setColor(this.c);
		g.drawLine(x0, y0, x, y);
		x0 = x;
		y0 = y;
	}

	/**
	 *
	 * @param c
	 */
	public void changerCouleur(Color c) {
		System.out.println(c);
		this.c = c;
	}
}

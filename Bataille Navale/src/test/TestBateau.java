/**
 * 
 */
package test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.*;

import exception.LongueurException;
import jeu.Bateau;

/**
 * @author BERNARD Hugo 'Tenebrosful'
 *
 */
public class TestBateau {
	
	static Bateau b;
	static Bateau b1;
	static Bateau b2;
	static Bateau b3;

	/**
	 * Fonction execut� avant tous les tests
	 * @throws java.lang.Exception
	 */
	@SuppressWarnings("javadoc")
	@Before
	public void setUpBeforeClass() throws Exception {
	}
	
	 /**
	 * Test d'un constructeur execut� normalement
	 * @throws LongueurException Lanc�e si la longueur du bateau est n�gative
	 */
	@Test
	 public void constructeurOK() throws LongueurException {
		 b = new Bateau(2, "Charle de Gaulle");
		 
		 assertEquals(2,b.getLongueur());
		 assertEquals("Charle de Gaulle",b.getNom());
		 assertEquals(0,b.getPosX());
		 assertEquals(0,b.getPosY());
		 assertEquals(null,b.getOrientation());
	 }
	
	/**
	 * Test d'un constructeur avec une longueur n�gative
	 * @throws LongueurException Lanc�e si la longueur du bateau est n�gative
	 */
	@Test(expected = LongueurException.class)
	public void constructeurLongueurNegative() throws LongueurException {
		b = new Bateau(-1,null);
	}
	
	/**
	 * Test d'un constructeur pour un nom vide ou null
	 * @throws LongueurException Lanc�e si la longueur du bateau est n�gative
	 */
	@Test
	public void constructeurNomVide() throws LongueurException{
		b1 = new Bateau(2,"");
		b2 = new Bateau(2,null);
		
		assertEquals(true,!b1.getNom().equals(""));
		assertEquals(true,b2.getNom()!=null);
	}
}

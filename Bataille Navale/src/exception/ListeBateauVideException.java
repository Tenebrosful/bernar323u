package exception;
/**
 * Exception concernant une liste de bateau vide
 */

/**
 * @author BERNARD Hugo 'Tenebrosful'
 *
 */
public class ListeBateauVideException extends Exception {

	/**
	 * Constructeur de ListeBateauVideException.java pour un message donn�
	 * @param message Message d'erreur de l'exception
	 */
	public ListeBateauVideException(String message) {
		super(message);
	}
}

package exception;
/**
 * Exception lorsqu'une case est d�j� occup�e par un bateau
 */

/**
 * @author BERNARD Hugo 'Tenebrosful'
 *
 */
public class CaseOccupeeException extends GrilleException {

	/**
	 * Constructeur de CaseOccupeeException.java pour un message donn�
	 * @param message Message d'erreur de l'exception
	 */
	public CaseOccupeeException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

}

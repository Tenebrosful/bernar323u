package exception;
/**
 * Exception concernant la taille d'une grille
 */

/**
 * @author BERNARD Hugo 'Tenebrosful'
 *
 */
public class TailleException extends GrilleException {

	/**
	 * Constructeur de CaseInexistanteException.java pour un message donn�
	 * @param message Message d'erreur de l'exception
	 */
	public TailleException(String message) {
		super(message);
	}

}

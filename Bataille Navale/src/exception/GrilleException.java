package exception;
/**
 * Exception concernant la grille
 */

/**
 * @author BERNARD Hugo 'Tenebrosful'
 *
 */
public class GrilleException extends Exception {

	/**
	 * Constructeur de GrilleException.java pour un message donn�
	 * @param message Message d'erreur de l'exception
	 */
	public GrilleException(String message) {
		super(message);
	}
}

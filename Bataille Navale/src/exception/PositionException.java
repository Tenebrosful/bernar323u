package exception;
/**
 * Exception concernant une position dans une grille
 */

/**
 * @author BERNARD Hugo 'Tenebrosful'
 *
 */
public class PositionException extends GrilleException {

	/**
	 * Constructeur de PositionException.java pour un message donn�
	 * @param message Message d'erreur de l'exception
	 */
	public PositionException(String message) {
		super(message);
	}
}

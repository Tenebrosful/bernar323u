package exception;
/**
 * Exception concernant une case inexistante
 */

/**
 * @author BERNARD Hugo 'Tenebrosful'
 *
 */
public class CaseInexistanteException extends GrilleException {

	/**
	 * Constructeur de CaseInexistanteException.java pour un message donn�
	 * @param message Message d'erreur de l'exception
	 */
	public CaseInexistanteException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

}

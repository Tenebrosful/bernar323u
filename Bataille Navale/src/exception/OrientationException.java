package exception;
/**
 * Exception concernant l'orientation d'un bateau
 */

/**
 * @author BERNARD Hugo 'Tenebrosful'
 *
 */
public class OrientationException extends BateauException {

	/**
	 * Constructeur de OrientationException.java pour un message donn�
	 * @param message Message d'erreur de l'exception
	 */
	public OrientationException(String message) {
		super(message);
	}

}

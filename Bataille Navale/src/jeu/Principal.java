package jeu;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import exception.CaseInexistanteException;
import exception.CaseOccupeeException;
import exception.ListeBateauVideException;
import exception.LongueurException;
import exception.OrientationException;
import exception.TailleException;

/**
 * Programme principal g�rant le d�roulement du jeu
 */

/**
 * @author BERNARD Hugo 'Tenebrosful'
 *
 */
public class Principal {

	private static Scanner sc = new Scanner(System.in);
	private static Joueur j1;
	private static Joueur j2;

	/**
	 * Correspond � un type de partie 0 = Aucune (Actuellement en menu) 1 = Partie
	 * Mono-joueur 2 = Partie Multi-joueur
	 */
	private static int codePartie = 0;

	/**
	 * M�thode principal g�rant le d�roulement du jeu
	 * 
	 * @param args (initulis�)
	 * @throws TailleException          Lanc�e lorsque la taille de la grille est
	 *                                  n�gative
	 * @throws LongueurException        lanc�e lorsque la longueur d'un bateau est
	 *                                  n�gatif
	 * @throws ListeBateauVideException Lanc�e lorsque la liste de bateau est vide
	 * @throws OrientationException     Lanc�e lorsque l'orientation n'est pas
	 *                                  valide
	 * @throws CaseOccupeeException     lanc�e lorsqu'une case est d�j� occup�e
	 * @throws CaseInexistanteException lanc�e lorsque la case n'existe pas
	 */
	public static void main(String[] args) throws TailleException, LongueurException, ListeBateauVideException,
			CaseInexistanteException, CaseOccupeeException, OrientationException {

		boolean menu = true;
		while (menu) {
			System.out.println("============\n" + "Bataille Navale\n" + "============");
			System.out.println("Menu principal");
			System.out.println("     1) Jeu mono-joueur");
			System.out.println("     2) Jeu � 2 joueurs");
			System.out.println("     3) Liste de noms des bateaux par d�faut");
			System.out.println("     4) Quitter le jeu");
			System.out.print("\nNum�ro de menu : ");

			switch (sc.nextInt()) {
			case 1:
				menuJeuMonoJoueur();
				break;

			case 2:
				jeuMultiJoueur();
				break;

			case 3:
				menuNomBateauDefaut();
				break;

			case 4:
				menu = false;
				break;

			default:
				System.out.println("\n   /!\\ La r�ponse est invalide, veuillez r��sayez /!\\\n");
				break;

			}

			codePartie = 0;
		}

	}

	/**
	 * Permet de sauvegarder la partie en cours
	 * 
	 * @param nomF Nom du fichier de sauvegarde
	 */
	public static void sauvegarder(String nomF) {

	}

	/**
	 * Permet de charge une partie � partir d'un fichier de sauvegarde
	 * 
	 * @param nomF Nom du fichier de sauvegarde � charger
	 */
	public static void charger(String nomF) {

	}

	/**
	 * Menu permettant de gerer les noms des bateaux par d�faut
	 */
	private static void menuNomBateauDefaut() {
		System.out.println("Cette fonctionabilit� n'a pas encore �t� implent�");
		sc.nextLine();
	}

	/**
	 * Menu du mode de jeu � un joueur
	 */
	public static void menuJeuMonoJoueur() {
		codePartie = 1;
		boolean menu = true;
		while (menu) {
			System.out.println("\n\n\n============Mode 1 joueur============\n");
			System.out.println("     1) Nouvelle partie");
			System.out.println("     2) Charger une partie");
			System.out.println("     3) Retour");
			System.out.print("\nNum�ro de menu : ");

			switch (sc.nextInt()) {
			case 1:
				setupMonoJoueur();
				break;

			default:
				System.out.println("R�ponse invalide");
				sc.nextLine();
				menu = false;
				break;
			}
		}
		System.out.println("Cette fonctionabilit� n'a pas encore �t� implent�");
		sc.nextLine();
	}

	/**
	 * Param�trage d'une partie � un seul joueur
	 */
	public static void setupMonoJoueur() {
		System.out.println("\n\n\n============Cr�ation d'une partie 1 joueur============\n");

		boolean entreeValide = false;
		while (!entreeValide) {
			System.out.print("\n     Veuillez indiquer la taille de la grille en x : ");
			int x = sc.nextInt();
			System.out.print("\n     Veuillez indiquer la taille de la grille en y : ");
			int y = sc.nextInt();
			try {
				Grille grille = new Grille(x, y);
				entreeValide = true;
			} catch (TailleException e) {
				System.out.println("\n   /!\\ La taille de la grille est invalide, veuillez r�essayer /!\\\n");
			}
		}

		int nbrBateau = 0;

		while (nbrBateau <= 0) {
			System.out.print("\n     Nombre de bateau : ");
			nbrBateau = sc.nextInt();

			if (nbrBateau <= 0)
				System.out.println("\n   /!\\ Le nombre de bateau est incorrect, veuillez r�essayer /!\\\n");
		}

		List<Bateau> listeBateauDepart = new ArrayList<Bateau>();

		for (int i = 1; i <= nbrBateau; ++i) {

			entreeValide = false;

			while (!entreeValide) {

				int longueur = 0;
				String nom = null;

				System.out.print("\n     Veuillez indiquer la longueur du bateau n�" + i);
				longueur = sc.nextInt();

				System.out.print("\n     Veuillez indiquer le nom du bateau n�" + i
						+ " (ou faites Entrer pour un par d�faut al�atoire) : ");
				nom = sc.nextLine();

				try {
					listeBateauDepart.add(new Bateau(longueur, nom));
					if (nom == null || nom.equals(""))
						System.out.println(listeBateauDepart.get(i-1).getNom());
					entreeValide = true;
				} catch (LongueurException e) {
					System.out.println("\n   /!\\ La longueur du bateau est incorrecte, veuillez r�essayer /!\\\n");
				}
			}
		}
	}

	/**
	 * Mode de jeu � plusieurs joueurs
	 */
	public static void jeuMultiJoueur() {
		codePartie = 2;
		System.out.println("Cette fonctionabilit� n'a pas encore �t� implent�");
		sc.nextLine();
	}
}

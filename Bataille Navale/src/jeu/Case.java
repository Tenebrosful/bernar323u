package jeu;
/**
 * Mod�lise une case du jeu
 */

/**
 * @author BERNARD Hugo 'Tenebrosful'
 *
 */
public class Case {
	
	/**
	 * Position en x de la case
	 */
	private int posX;
	
	/**
	 * Position en y de la case
	 */
	private int posY;
	
	/**
	 * Bateau pr�sent sur la case
	 */
	private Bateau bateau;
	
	/**
	 * Indique si la case a �t� d�j� cibl� par un tir
	 */
	private boolean impact;
	
	/**
	 * Constructeur de Case.java pour une position (x;y) donn�e
	 * @param posX Position en x de la case
	 * @param posY Position en y de la case
	 */
	public Case(int posX, int posY) {
		this.posX = posX;
		this.posY = posY;
	}
	/**
	 * Teste si les coordonn�es en param�tre correspondent � la case
	 * @param posX Position en x � tester
	 * @param posY Position en y � tester
	 * @return True si la case correspond aux coordonn�es en param�tre
	 */
	public boolean aPourCoordonnee(int posX, int posY) {
		return (this.posX == posX && this.posY == posY);
	}
	/**
	 * Retourne bateau
	 * @return bateau
	 */
	public Bateau getBateau() {
		return bateau;
	}
	/**
	 * Retourne posX
	 * @return posX
	 */
	public int getPosX() {
		return posX;
	}
	/**
	 * Retourne posY
	 * @return posY
	 */
	public int getPosY() {
		return posY;
	}
	/**
	 * Retourne impact
	 * @return impact
	 */
	public boolean isImpact() {
		return impact;
	}
	/**
	 * Teste si la case est occup�e par un bateau
	 * @return True si la case est occup�e par un bateau
	 */
	public boolean isOccupee() {
		return this.bateau != null;
	}
	
	/**
	 * Modifie le bateau pr�sent sur la case
	 * @param bateau Nouvelle valeur de bateau 
	 */
	public void setBateau(Bateau bateau) {
		this.bateau = bateau;
	}
	/**
	 * Modifie l'�tat d'impact du bateau pr�sent sur la case
	 * @param impact Nouvelle valeur de impact 
	 */
	public void setImpact(boolean impact) {
		this.impact = impact;
	}
	
	@Override
	public String toString() {
		return "|" + this.getPosX() + ";" + this.getPosY() + ";" + this.isOccupee() + ";" + this.isImpact() + "|";
	}
	

}

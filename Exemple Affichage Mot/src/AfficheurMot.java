import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

/**
 * 
 */

public class AfficheurMot extends JPanel {
	
	private int pos_x, pos_y;
	private String mot;
	
	/**
	 * Constructeur de AfficheurMot.java
	 */
	public AfficheurMot() {
		this.mot = "";
		this.pos_x = 0;
		this.pos_y = 0;
		
		final JCheckBox maj = new JCheckBox("Majuscules");
		this.add(maj);
		
		JRadioButton b1 = new JRadioButton("Sens direct");
		JRadioButton b2 = new JRadioButton("Sens inverse");
		
		ButtonGroup bg = new ButtonGroup();
		bg.add(b1);
		bg.add(b2);
		b1.setSelected(true);
		this.add(b1);
		this.add(b2);
		
		this.add(new JLabel("Mot � afficher : "));
		final JTextField tf = new JTextField("");
		tf.setColumns(10);
		this.add(tf);
		
		this.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				pos_x = e.getX();
				pos_y = e.getY();
				mot = tf.getText();
				
				if(maj.isSelected())
					mot = mot.toUpperCase();
				
				if(b2.isSelected())
					mot = new StringBuffer(mot).reverse().toString();
				
				repaint();
			}
		});
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawString(mot, pos_x, pos_y);
	}
}

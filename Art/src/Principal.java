import java.awt.Dimension;

import javax.swing.JFrame;

/**
 * 
 */

/**
 * @author BERNARD Hugo 'Tenebrosful'
 *
 */
public class Principal {

	/**
	 *
	 * @param args
	 */
	public static void main(String[] args) {
		JFrame fenetre = new JFrame("Vive les ovales");
		fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		AfficheurArt art = new AfficheurArt();
		art.setPreferredSize(new Dimension(600, 600));
		fenetre.setContentPane(art);
		fenetre.pack();
		fenetre.setVisible(true);
	}

}

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * 
 */

/**
 * @author BERNARD Hugo 'Tenebrosful'
 *
 */
public class AfficheurArt extends JPanel {

	private int nbrOvale;

	/**
	 * Constructeur de AfficheurArt.java
	 */
	public AfficheurArt() {
		this.nbrOvale = 0;

		this.setLayout(new BorderLayout());

		dessinOvale dessin = new dessinOvale();
		this.add(dessin, BorderLayout.CENTER);

		JPanel jpSaisie = new JPanel();

		jpSaisie.add(new JLabel("Nombre d'oval : "));

		final JTextField jtf = new JTextField();
		jtf.setColumns(4);
		jpSaisie.add(jtf);

		JButton jb = new JButton("Valider");
		jb.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				nbrOvale = Integer.parseInt(jtf.getText());
				dessin.repaint();
			}

		});
		jpSaisie.add(jb);

		this.add(jpSaisie, BorderLayout.SOUTH);

	}

	/**
	 * @author BERNARD Hugo 'Tenebrosful'
	 *
	 */
	public class dessinOvale extends JPanel {

		/**
		 *
		 * @param g
		 */
		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			int h = this.getHeight();
			int w = this.getWidth();

			for (int i = 0; i < nbrOvale; ++i) {

				// Selection d'une couleur al�atoire
				g.setColor(new Color((int) (Math.rint(Math.random() * 255)), (int) (Math.rint(Math.random() * 255)),
						(int) (Math.rint(Math.random() * 255))));

				// Dessin de l'oval � une position al�atoire et de taille al�atoire
				g.fillOval((int) (Math.rint(Math.random() * (w - 1))), (int) (Math.rint(Math.random() * (h - 1))),
						(int) (Math.rint(Math.random() * (h - 1))), (int) (Math.rint(Math.random() * (h - 1))));
			}
		}
	}
}

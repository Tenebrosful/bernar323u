import java.awt.Dimension;

import javax.swing.JFrame;

/**
 * 
 */

public class AffichageDiagonaleComponants {

	/**
	 *
	 * @param args Arguments d'entr�s
	 */
	public static void main(String[] args) {
		JFrame fenetre = new JFrame("Affichage d'une diagonale rouge");
		fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		DiagonaleComposants dessin = new DiagonaleComposants();
		dessin.setPreferredSize(new Dimension(800,800));
		fenetre.setContentPane(dessin);
		fenetre.pack();
		fenetre.setVisible(true);
	}

}

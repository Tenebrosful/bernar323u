import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;

/**
 * 
 */

public class DiagonaleComposants extends JPanel {
	Color couleur;

	/**
	 * Constructeur de DiagonaleComposants.java
	 */
	public DiagonaleComposants() {
		couleur = Color.RED;
		
		JButton b = new JButton("Effacer");
		b.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				getGraphics().clearRect(0, 0, getWidth(), getHeight());
			}
		});
		this.add(b);
		
		final JComboBox<String> c = new JComboBox<String>(new String[] {"Rouge","Vert","Jaune","Bleu"});
		c.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				switch (c.getSelectedIndex()) {
				case 0 : couleur = Color.RED; break;
				case 1 : couleur = Color.GREEN; break;
				case 2 : couleur = Color.YELLOW; break;
				case 3 : couleur = Color.BLUE; break;
				default : couleur = Color.BLACK;
				}
				repaint();
			}
		});
		this.add(c);
	}
	
	/**
	 *
	 * @param g Zone de dessin
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		int h = getHeight();
		int w = getWidth();
		g.setColor(couleur);
		g.drawLine(0, 0, w-1, h-1);
	}
}

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * 
 */

/**
 * @author bernar323u
 *
 */
public class Exo24 {

	/**
	 * @param args
	 * @throws SQLException
	 */
	public static void main(String[] args) throws SQLException {
		Connection c = DriverManager.getConnection("jdbc:oracle:thin:@charlemagne.iutnc.univ-lorraine.fr:1521:infodb",
				"bernar323u", "mewmew");

		PreparedStatement psListeNotationInternaute = c.prepareStatement(
				"SELECT PAYSINT, NOMINT, PRENOMINT, EMAIL, count(NOTE) nbrNote, avg(NOTE) moyenne FROM NOTATION natural join INTERNAUTE group by email, nomint, prenomint, paysint ORDER BY PAYSINT, NOMINT, PRENOMINT",
				ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
		PreparedStatement psListeNotationFilm = c.prepareStatement("SELECT TITRE, NOTE FROM NOTATION NATURAL JOIN FILM WHERE EMAIL = ?",
				ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);

		ResultSet rsListeNotationInternaute = psListeNotationInternaute.executeQuery();

		rsListeNotationInternaute.last();

		if (rsListeNotationInternaute.getRow() == 0) {
			System.out.println("Aucun internaute");
		} else {
			rsListeNotationInternaute.beforeFirst();
			String lastPaysInt = "";
			while (rsListeNotationInternaute.next()) {
				if (!rsListeNotationInternaute.getString("paysInt").equals(lastPaysInt)) {
					System.out.println("\n\n" + rsListeNotationInternaute.getString("paysInt"));
					lastPaysInt = rsListeNotationInternaute.getString("paysInt");
				}

				System.out.println("\n   " + rsListeNotationInternaute.getString("nomInt") + " "
						+ rsListeNotationInternaute.getString("prenomInt") + " "
						+ rsListeNotationInternaute.getInt("nbrNote") + " "
						+ rsListeNotationInternaute.getDouble("moyenne"));

				psListeNotationFilm.setString(1, rsListeNotationInternaute.getString("email"));
				ResultSet rsListeNotationFilm = psListeNotationFilm.executeQuery();

				rsListeNotationFilm.last();

				if (rsListeNotationFilm.getRow() == 0) {
					System.out.println("Aucune note");
				} else {
					rsListeNotationFilm.beforeFirst();
					while(rsListeNotationFilm.next()) {
						System.out.println("     - " + rsListeNotationFilm.getString("titre") + " " + rsListeNotationFilm.getString("note"));
					}
				}

			}
		}

	}

}

import java.sql.*;

/**
 * Programme principal r�gissant le d�roulement du jeu
 */

/**
 * @author BERNARD Hugo 'Tenebrosful'
 *
 */
public class Exo11 {

	/**
	 *
	 * @param args
	 * @throws SQLException
	 */
	public static void main(String[] args) throws SQLException {
		Connection c = DriverManager.getConnection("jdbc:oracle:thin:@charlemagne.iutnc.univ-lorraine.fr:1521:infodb",
				"bernar323u", "mewmew");
		PreparedStatement ps = c
				.prepareStatement("SELECT titre, genre, paysfilm, entree FROM FILM WHERE entree > 500000");
		ResultSet res = ps.executeQuery();
		System.out.println("Titre   |   Genre   |    Pays   |   Entr�e");
		while (res.next()) {
			System.out.println(res.getString("titre") + " | " + res.getString("genre") + " | "
					+ res.getString("paysfilm") + " | " + res.getInt("entree"));
		}

		res.close();
		c.close();
	}

}

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * 
 */

/**
 * @author BERNARD Hugo 'Tenebrosful'
 *
 */
public class Exo25 {

	/**
	 *
	 * @param args
	 * @throws SQLException
	 */
	public static void main(String[] args) throws SQLException {
		Connection c = DriverManager.getConnection("jdbc:oracle:thin:@charlemagne.iutnc.univ-lorraine.fr:1521:infodb",
				"bernar323u", "mewmew");

		PreparedStatement psListeNotationInternaute = c.prepareStatement(
				"SELECT NOMINT, PRENOMINT, EMAIL, TITRE, NOTE FROM NOTATION NATURAL JOIN INTERNAUTE NATURAL JOIN FILM ORDER BY NOMINT, PRENOMINT, TITRE",
				ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);

		ResultSet rsListeNotationInternaute = psListeNotationInternaute.executeQuery();

		rsListeNotationInternaute.last();
		if (rsListeNotationInternaute.getRow() == 0) {
			System.out.println("Aucun internaute existant");
		} else {
			rsListeNotationInternaute.beforeFirst();
			int numNote = 0;
			String lastEmail = "";
			
			while(rsListeNotationInternaute.next()) {
				if(!rsListeNotationInternaute.getString("email").equals(lastEmail)) {
					System.out.println("\n\n" + rsListeNotationInternaute.getString("nomInt") + " " + rsListeNotationInternaute.getString("prenomInt") + " a not� les films :");
					numNote = 0;
					lastEmail = rsListeNotationInternaute.getString("email");
				}
				
				numNote++;
				
				System.out.println("  " + numNote + " � '" + rsListeNotationInternaute.getString("titre") + "' avec la note de " + rsListeNotationInternaute.getInt("note"));
			}
		}
	}

}
